PYI=python
SERVICE_FLAG=
SCSS_PATH=scss
CSS_PATH=css


install_deps:
	@bower install

service:
	$(PYI) -m SimpleHTTPServer

css:
	mkdir -p $(SCSS_PATH) $(CSS_PATH)
	@sass --update $(SCSS_PATH):$(CSS_PATH) --style compressed

css_live:
	mkdir -p $(SCSS_PATH) $(CSS_PATH)
	@sass --watch $(SCSS_PATH):$(CSS_PATH) --style compressed
