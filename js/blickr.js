'use strict';

if (window.Polymer === undefined) {
    throw 'shiroyuki.blickr.MissingPolymer';
}

if (window.$ === undefined && window.jQuery === undefined) {
    throw 'shiroyuki.blickr.MissingJQuery';
}

function Blickr(context) {
    this.activated = false;
    this.context = context;
    this.options = {
        includeActions: true
    };

    this.actionsElement = null;

    $(this.context).on('submit', $.proxy(this.onSubmit, this));
}

$.extend(Blickr.prototype, {
    guidElementId: 0,
    usedModules: {},
    ignoredElements: {
        'blickr-inserter': true,
        'blickr-actions':  true
    },

    setOptions: function (options) {
        $.extend(this.options, options);
    },

    activate: function ($referencePoint, how) {
        if (this.activated) {
            throw 'shiroyuki.blickr.AlreadyInitiated';
        }

        this.activated = true;
    },

    enableControls: function () {
        this.addOne('actions');
        this.actionsElement = this.context.querySelector(this._getElementName('actions'));

        this.actionsElement.addEventListener('cancel', $.proxy(this.onCancel, this));
        this.actionsElement.addEventListener('save',   $.proxy(this.onSave, this));
    },

    on: function (type, handler) {
        this.context.addEventListener('blickr.' + type, handler);
    },

    trigger: function (type, detail) {
        var dispatchingEvent = new CustomEvent('blickr.' + type, { detail: detail });

        this.context.dispatchEvent(dispatchingEvent);
    },

    add: function (kind, options, referencePointId) {
        var target;

        options = options || {};

        if (referencePointId) {
            this.addOne('inserter', {}, referencePointId);
        }

        options['class'] = 'element';
        target = this.addOne(kind, options, referencePointId);

        if (!referencePointId) {
            this.addOne('inserter', {}, referencePointId);
        }

        this.trigger('element.added', { elementId: target.childId });

        return target;
    },

    addOne: function (kind, options, referencePointId) {
        var optionStrings = [],
            key,
            $context      = $(this.context),
            elementName   = this._getElementName(kind),
            guidElementId = this.guidElementId++,
            selector      = this._getSelectorForElement(kind, guidElementId),
            target,
            elementCode
        ;

        for (key in options) {
            optionStrings.push(key + '="' + options[key] + '"');
        }

        elementCode = '<' + elementName + ' childId="' + guidElementId
            + '"' + optionStrings.join(' ') + '></' + elementName + '>';

        if (referencePointId) {
            $(this.context.querySelector(this._getSelectorForElement('inserter', referencePointId))).after(elementCode);
        } else if (this.actionsElement) {
            $(this.actionsElement).before(elementCode);
        } else {
            $context.append(elementCode);
        }

        // Don't use jQuery to bind the event handler as it removes the "detail" property from the event object.
        target = this.context.querySelector(selector);
        target.addEventListener('add',     $.proxy(this.onInserterAdd,    this));
        target.addEventListener('removed', $.proxy(this.onElementDeleted, this));

        // Set the default options.
        if (this.options[kind]) {
            for (key in this.options[kind]) {
                console.log(kind, key, this.options[kind][key]);
                target[key] = this.options[kind][key];
            }
        }

        return target;
    },

    getData: function () {
        var self = this,
            data = []
        ;

        $(this.context).children().each(function (i, n) {
            var nodeName = String(n.nodeName).toLowerCase();

            if (self.ignoredElements[nodeName]) {
                return;
            }

            data.push({
                priority: i,
                element: nodeName,
                display: n.elementType,
                content: n.value
            });
        });

        return data;
    },

    onInserterAdd: function (e) {
        var kind       = e.detail.kind,
            originId   = e.detail.originId
        ;

        this.add(kind, {}, originId);
    },

    onElementDeleted: function (e) {
        this.trigger('element.deleted', e.detail);
    },

    onSubmit: function (e) {
        var data = this.getData(),
            dispatchingEvent;

        e.preventDefault();

        this.trigger('submit', { data: data });
    },

    onSave: function (e) {
        $(this.context).trigger('submit');
    },

    onCancel: function (e) {
        alert('Not implemented');
    },

    _getElementName: function (kind) {
        return 'blickr-' + kind;
    },

    _getSelectorForElement: function (kind, guidElementId) {
        return this._getElementName(kind) + '[childId="' + guidElementId + '"]'
    }
});